# public-gpg-keys
My rotating set of public GPG/PGP keys used for signing commits, packages, releases etc.

✔️ Fingerprint: `A6A4 1A3B DE6C 5667 4D35  9242 AD6D C3BA 2DF4 E9A4` (2021-03-13)
